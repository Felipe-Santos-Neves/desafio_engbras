const { response } = require('express');
const express = require('express');

const app = new express();

// Criando rota GET
app.get('/', (require, response) => {
    response.send('Hello Word');
});

// Criando a rota POST
app.use(express.json());

app.post('/', (require, response) => {
    response.send(require.body);
})

// Executar o servidor na porta 3001
app.listen(3001);