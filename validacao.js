const app = new Vue({
    el: "#app",
    data: {
        nome: null,
        senha: null,
        errors: []
    },
    methods: {
        checkForm: function (e) {

            this.errors = [];

            if(!this.nome || !this.senha) {
                this.errors.push('Por favor, preencha todos os campos');
            } else {
                alert("Dados inseridos corretamente");
            }

            e.preventDefault();
        }
    }
})